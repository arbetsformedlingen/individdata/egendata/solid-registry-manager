# Egendata Interop

Utility for managing solid registries.

## Usage

```typescript

// Inspired by Prisma. https://www.prisma.io/

import { Registry } from 'egendata-interop';

const webid = new URL('https://identity-provider.localhost/egendata/profile/card#me');
const dataRegistry = await new RegistrySet(webid).dataRegistries()[0];

const registration = await dataRegistry.registrations.find('DocumentTypeMetadata'); // string == short format, url == long format

registration.instances.find({ pred: '', obj: '' }) // return N3.Store

registration.instances.find({ pred: '' })

registration.instances.find({ obj: '' })



const data1 = await registration.filter(???); // Some kind of filter function?

const data2 = await registry.registrations.find('...').filter(''); // Doing it all in one row.


const newRegistration = await registry.dataRegistration.add(string | URL);
await registry.dataRegistration.delete(string | URL);

const newData1 = await registration.add(???);
await registration.delete(???);
```

## Development

### Starting the server

To bootstrap the server run `npm run bootstrap`. This gives you a user with user id `egendata@example.com` and password `qwerty`. The server can be started with `npm run server`, on `http://localhost:3000`.

The users webid profile is `http://localhost:3000/egendata/profile/card#me`.

## Data Structure

```mermaid
flowchart LR
    Profile[Profile\n.../egendata/profile/card#me\nhasRegistrySet]
    RegistrySet[RegistrySet\n.../egendata/registryset\nhasDataRegistry]
    DataRegistry[DataRegistry\n.../egendata/data/registry\nhasDataRegistration]
    DataRegistrationA["DataRegistration\n.../egendata/data/f33a...\nshapetree:DocumentTypeMetadata\ncontains[]"]
    DataRegistrationB["DataRegistration\n.../egendata/data/dfb7...\nshapetree:RequestMetadata\ncontains[]"]
    DataA[Data instance\n.../egendata/data/bded...]
    DataB[Data instance\n.../egendata/data/39b4...]
    DataC[Data instance\n.../egendata/data/c0ac...]

    Profile  -->|hasRegistrySet| RegistrySet
    RegistrySet -->|hasDataRegistry| DataRegistry

    DataRegistry -->|hasDataRegistration| DataRegistrationA
    DataRegistry -->|hasDataRegistration| DataRegistrationB

    DataRegistrationA -->|contains| DataA
    DataRegistrationA -->|contains| DataB

    DataRegistrationB -->|contains| DataC
```

getDataRegistry(webid) => DataRegistry (might have to create RegistrySet and DataRegistry (empty))

## References

https://basarat.gitbook.io/typescript/library
