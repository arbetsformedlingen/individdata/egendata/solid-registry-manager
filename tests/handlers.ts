import { rest } from 'msw';

export const handlers = [
  rest.get('http://pod.example.com/egendata/profile/card', (req, res, ctx) => {
    return res(
      ctx.text(`@prefix foaf: <http://xmlns.com/foaf/0.1/>.
    @prefix solid: <http://www.w3.org/ns/solid/terms#>.
    @prefix space: <http://www.w3.org/ns/pim/space#>.
    @prefix interop: <http://www.w3.org/ns/solid/interop#>.

    <>
      a foaf:PersonalProfileDocument;
      foaf:maker <http://pod.example.com/egendata/profile/card#me>;
      foaf:primaryTopic <http://pod.example.com/egendata/profile/card#me>.

    <http://pod.example.com/egendata/profile/card#me>
      a foaf:Organization;
      solid:oidcIssuer <http://pod.example.com/>;
      space:storage <http://pod.example.com/egendata/>;
      a interop:Agent;
      interop:hasRegistrySet <http://pod.example.com/egendata/registryset>.`)
    );
  }),
  rest.get('http://pod.example.com/egendata-noregistryset/profile/card', (req, res, ctx) => {
    return res(
      ctx.text(`@prefix foaf: <http://xmlns.com/foaf/0.1/>.
    @prefix solid: <http://www.w3.org/ns/solid/terms#>.
    @prefix space: <http://www.w3.org/ns/pim/space#>.
    @prefix interop: <http://www.w3.org/ns/solid/interop#>.

    <>
      a foaf:PersonalProfileDocument;
      foaf:maker <http://pod.example.com/egendata/profile/card#me>;
      foaf:primaryTopic <http://pod.example.com/egendata/profile/card#me>.

    <http://pod.example.com/egendata/profile/card#me>
      a foaf:Organization;
      solid:oidcIssuer <http://pod.example.com/>;
      space:storage <http://pod.example.com/egendata/>;
      a interop:Agent.`)
    );
  }),
  rest.get('http://pod.example.com/egendata/registryset', (req, res, ctx) => {
    return res(
      ctx.text(`@prefix interop: <http://www.w3.org/ns/solid/interop#> .

    <>
      a interop:RegistrySet;
      interop:hasDataRegistry <http://pod.example.com/egendata/data/registry>.`)
    );
  }),
  rest.get('http://pod.example.com/egendata/data/registry', (req, res, ctx) => {
    return res(
      ctx.text(
        `@prefix interop: <http://www.w3.org/ns/solid/interop#> .
    @prefix data: <http://pod.example.com/egendata/data/> .

    <>
      a interop:DataRegistry;
      interop:hasDataRegistration 
        data:f33ae00d-8f62-46ed-aac8-40335886e7e2, 
        data:dfb7d3f4-5417-46be-aca4-ee0bc1a3bff4.`
      )
    );
  }),
  rest.get('http://pod.example.com/egendata/data/dfb7d3f4-5417-46be-aca4-ee0bc1a3bff4', (req, res, ctx) => {
    return res(
      ctx.text(`@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
      @prefix ldp: <http://www.w3.org/ns/ldp#>.
      @prefix interop: <http://www.w3.org/ns/solid/interop#>.
      @prefix shapetree: <http://data.example.com/shapetree/pm#>.
      
      <> a interop:DataRegistration;
        interop:registeredBy <http://localhost:3000/egendata/profile/card#me>;
        interop:registeredWith <http://localhost:3000/egendata/profile/card#me>;
        interop:registeredAt "2020-04-04T20:15:47.000Z"^^xsd:dateTime;
        interop:updatedAt "2020-04-04T21:11:33.000Z"^^xsd:dateTime;
        interop:registeredShapeTree shapetree:RequestMetadata;
      
        ldp:contains
          <c0ac1b0e-3bdd-4429-8c2a-b898ae8216a0>.`)
    );
  }),
  rest.get('http://pod.example.com/egendata/data/f33ae00d-8f62-46ed-aac8-40335886e7e2', (req, res, ctx) => {
    return res(
      ctx.text(`@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
      @prefix ldp: <http://www.w3.org/ns/ldp#>.
      @prefix interop: <http://www.w3.org/ns/solid/interop#>.
      @prefix shapetree: <http://data.example.com/shapetree/pm#>.
      
      <> a interop:DataRegistration;
        interop:registeredBy <http://localhost:3000/egendata/profile/card#me>;
        interop:registeredWith <http://localhost:3000/egendata/profile/card#me>;
        interop:registeredAt "2020-04-04T20:15:47.000Z"^^xsd:dateTime;
        interop:updatedAt "2020-04-04T21:11:33.000Z"^^xsd:dateTime;
        interop:registeredShapeTree shapetree:DocumentTypeMetadata;
        
        ldp:contains
          <bded8341-0a63-4634-abe4-3627f4e6cab7>,
          <39b45f09-aff1-475f-9384-cc5172c70662>.`)
    );
  }),
  rest.get('http://pod.example.com/egendata/data/c0ac1b0e-3bdd-4429-8c2a-b898ae8216a0', (req, res, ctx) => {
    return res(
      ctx.text(`@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
      @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
      @prefix xml: <http://www.w3.org/XML/1998/namespace>.
      @prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
      @prefix ldp: <http://www.w3.org/ns/ldp#>.
      @prefix egendata: <https://egendata.se/core/v1#>.
      
      <> a <egendata:RequestMetadata>;
        egendata:purpose "Some scary purpose.";
        egendata:consentText "Lorem ipsum ...";  # <-- Allow handlebars syntax?
        egendata:consentCheck "Check 1";
        egendata:consentCheck "Check 2";
        egendata:created_at "2021-04-04T20:15:47.000Z"^^xsd:dateTime.`)
    );
  }),
  rest.get('http://pod.example.com/egendata/data/bded8341-0a63-4634-abe4-3627f4e6cab7', (req, res, ctx) => {
    return res(
      ctx.text(`@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
      @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
      @prefix xml: <http://www.w3.org/XML/1998/namespace>.
      @prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
      @prefix ldp: <http://www.w3.org/ns/ldp#>.
      @prefix egendata: <https://egendata.se/core/v1#>.
      
      <> a <egendata:DocumentTypeMetadata>;
        egendata:documentType egendata:JobSeekerRegistrationStatus;
        egendata:documentTitle "Arbetslöshetsintyg";
        egendata:created_at "2021-04-04T20:15:47.000Z"^^xsd:dateTime.`)
    );
  }),
  rest.get('http://pod.example.com/egendata/data/39b45f09-aff1-475f-9384-cc5172c70662', (req, res, ctx) => {
    return res(
      ctx.text(`@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
      @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
      @prefix xml: <http://www.w3.org/XML/1998/namespace>.
      @prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
      @prefix ldp: <http://www.w3.org/ns/ldp#>.
      @prefix egendata: <https://egendata.se/core/v1#>.
      
      <> a <egendata:DocumentTypeMetadata>;
        egendata:documentType egendata:SomeOtherDocument;
        egendata:documentTitle "Ett annat intyg";
        egendata:created_at "2021-04-04T20:15:47.000Z"^^xsd:dateTime.`)
    );
  }),
];
