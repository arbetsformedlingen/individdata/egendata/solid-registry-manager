import { afterEach, describe, expect, it, vi } from 'vitest';
import { Registration } from '../../src/Registration';

import * as N3 from 'n3';

const mockParser = { parse: vi.fn() };
const mockStore = { getQuads: vi.fn() } as any;

vi.mock('n3');

const spyParser = vi.spyOn(N3, 'Parser').mockImplementation(() => mockParser);
const spyStore = vi.spyOn(N3, 'Store').mockImplementation(() => mockStore);

describe('Registration', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('should create a new instance', () => {
    const url = new URL('https://example.com/some/path/to/registration');

    const mockFetch = vi.fn();

    const registration = new Registration(url, mockFetch);

    expect(registration).toBeDefined();
  });

  it('should initialize', async () => {
    const url = new URL('https://example.com/some/path/to/registration');
    const shapeTreeURL = new URL('https://example.com/some/path');

    const mockFetch = vi.fn();
    mockFetch.mockResolvedValue({ ok: true, text: vi.fn() });

    mockStore.getQuads.mockReturnValueOnce([{ object: { value: shapeTreeURL.toString() } }]);

    const registration = new Registration(url, mockFetch);
    await registration.init();

    expect(mockFetch).toBeCalledTimes(1);
    expect(mockFetch).toBeCalledWith(url, { headers: { Accept: 'text/turtle' } });
    expect(mockParser.parse).toBeCalledTimes(1);

    expect(registration._store).toBe(mockStore);
    expect(registration._registeredShapeTree).toStrictEqual(shapeTreeURL);
  });

  it('should only initialize once', async () => {
    const url = new URL('https://example.com/some/path/to/registration');
    const shapeTreeURL = new URL('https://example.com/some/path');

    const mockFetch = vi.fn();
    mockFetch.mockResolvedValue({ ok: true, text: vi.fn() });

    mockStore.getQuads.mockReturnValueOnce([{ object: { value: shapeTreeURL.toString() } }]);

    const registration = new Registration(url, mockFetch);
    await registration.init();

    expect(mockFetch).toBeCalledTimes(1);
    expect(mockFetch).toBeCalledWith(url, { headers: { Accept: 'text/turtle' } });
    expect(mockParser.parse).toBeCalledTimes(1);

    expect(registration._store).toBe(mockStore);
    expect(registration._registeredShapeTree).toStrictEqual(shapeTreeURL);

    await registration.init();
    expect(mockFetch).toBeCalledTimes(1);
  });

  describe('children.find', () => {
    it('should find registration instance', async () => {
      const url = new URL('https://example.com/some/path/to/registration');
      const shapeTreeURL = new URL('https://example.com/some/path');
      const instanceURL = new URL('https://example.com/some/instance');

      const mockFetch = vi.fn();
      mockFetch.mockResolvedValue({ ok: true, text: vi.fn() });

      mockStore.getQuads.mockReturnValueOnce([{ object: { value: shapeTreeURL.toString() } }]);
      mockStore.getQuads.mockReturnValueOnce([{ object: { value: instanceURL.toString() } }]);
      mockStore.getQuads.mockReturnValueOnce([{}]);

      const registration = new Registration(url, mockFetch);

      const instances = await registration.children.find({
        pred: 'https://example.com/predicate',
        obj: 'https://example.com/object',
      });

      expect(instances).toHaveLength(1);

      expect(mockFetch).toBeCalledWith(instanceURL);
    });
  });
});
