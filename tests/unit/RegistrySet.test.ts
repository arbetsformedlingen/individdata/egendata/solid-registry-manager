import { describe, expect, it, vi } from 'vitest';
import { RegistrySet } from '../../src/RegistrySet';

import * as N3 from 'n3';

const mockGetQuads = vi.fn();

vi.mock('n3', async () => {
  const actual = await vi.importActual<typeof import('n3')>('n3');

  return {
    ...actual,
    Parser: vi.fn().mockImplementation(() => ({ parse: vi.fn() })),
    Store: vi.fn().mockImplementation(() => ({ getQuads: mockGetQuads })),
  };
});
vi.mock('../../src/templates', () => ({ default: vi.fn(), registrySetTemplate: vi.fn() }));
vi.mock('../../src/Registry', () => ({ default: vi.fn(), Registry: vi.fn() }));

describe('RegistrySet', () => {
  it('should create a new instance', () => {
    const url = new URL('https://example.com/some/path/to/profile');
    const mockFetch = vi.fn();

    const registrySet = new RegistrySet(url, mockFetch);

    expect(registrySet).toBeDefined();
    expect(N3.Parser).toHaveBeenCalledOnce();
    expect(N3.Parser).toBeCalledWith({ baseIRI: 'https://example.com/some/path/to/profile' });
  });

  it('should initialize', async () => {
    const { quad, namedNode } = N3.DataFactory;

    const url = new URL('https://example.com/some/path/to/profile');
    const registrySetURL = new URL('http://example.com/registrySet');
    const mockFetch = vi.fn();
    mockFetch.mockResolvedValue({ ok: true, text: vi.fn() });

    mockGetQuads.mockReturnValue([
      quad(
        namedNode(url.toString()),
        namedNode('http://www.w3.org/ns/solid/interop#hasRegistrySet'),
        namedNode(registrySetURL.toString())
      ),
    ]);

    const registrySet = new RegistrySet(url, mockFetch);

    await registrySet.init();

    expect(mockFetch).toBeCalledWith(url, { headers: { Accept: 'text/turtle' } });
    expect(mockFetch).toBeCalledWith(registrySetURL);

    expect(registrySet._store).toBeDefined();
  });

  it('should only initialize once', async () => {
    const { quad, namedNode } = N3.DataFactory;

    const url = new URL('https://example.com/some/path/to/profile');
    const registrySetURL = new URL('http://example.com/registrySet');
    const mockFetch = vi.fn();
    mockFetch.mockResolvedValue({ ok: true, text: vi.fn() });

    mockGetQuads.mockReturnValue([
      quad(
        namedNode(url.toString()),
        namedNode('http://www.w3.org/ns/solid/interop#hasRegistrySet'),
        namedNode(registrySetURL.toString())
      ),
    ]);

    const registrySet = new RegistrySet(url, mockFetch);

    await registrySet.init();

    expect(mockFetch).toBeCalledTimes(2);
    expect(mockFetch).toBeCalledWith(url);
    expect(mockFetch).toBeCalledWith(registrySetURL);

    expect(registrySet._store).toBeDefined();

    await registrySet.init();

    expect(mockFetch).toBeCalledTimes(2);
  });

  it('should call createRegistrySet when initializing, if Registry Set is missing', async () => {
    const url = new URL('https://example.com/some/path/to/profile');
    const mockFetch = vi.fn();
    mockFetch.mockResolvedValue({ ok: true, text: vi.fn() });
    mockGetQuads.mockReturnValue([]);

    const mockCreateRegistrySet = vi.fn().mockResolvedValue(new URL('https://example.com/some/path/to/registrySet'));

    const registrySet = new RegistrySet(url, mockFetch);

    registrySet.createRegistrySet = mockCreateRegistrySet;

    await registrySet.init();

    expect(mockFetch).toBeCalledWith(url);
    expect(mockCreateRegistrySet).toBeCalled();
  });

  it('should throw if Registry Set can not be read', async () => {
    const url = new URL('https://example.com/some/path/to/profile');
    const mockFetch = vi.fn();
    mockFetch.mockResolvedValueOnce({ ok: true, text: vi.fn() });
    mockFetch.mockResolvedValueOnce({ ok: false });

    const registrySet = new RegistrySet(url, mockFetch);
    registrySet.createRegistrySet = vi.fn();

    await expect(registrySet.init()).rejects.toThrow('Failed to read Registry Set');
  });

  describe('createRegistrySet', () => {
    it('should create Registry Set and update profile', async () => {
      const url = new URL('https://example.com/some/path/to/profile');
      const expectedRegistrySetURL = new URL('https://example.com/some/path/to/registryset');

      const mockFetch = vi.fn();
      mockFetch.mockResolvedValue({ ok: true });

      const registrySet = new RegistrySet(url, mockFetch);

      const registrySetURL = await registrySet.createRegistrySet();

      expect(registrySetURL).toStrictEqual(expectedRegistrySetURL);
      expect(mockFetch).toHaveBeenCalledWith(expectedRegistrySetURL, {
        headers: { 'Content-Type': 'text/turtle' },
        method: 'PUT',
      });
      expect(mockFetch).toHaveBeenCalledWith(url, {
        body: 'INSERT DATA { <https://example.com/some/path/to/profile> <http://www.w3.org/ns/solid/interop#hasRegistrySet> <https://example.com/some/path/registryset>.}',
        headers: { 'Content-Type': 'application/sparql-update' },
        method: 'PATCH',
      });
    });

    it('should throw if Registry Set can not be written', async () => {
      const url = new URL('https://example.com/some/path/to/profile');

      const mockFetch = vi.fn();
      mockFetch.mockResolvedValue({ ok: false });

      const registrySet = new RegistrySet(url, mockFetch);

      await expect(registrySet.createRegistrySet()).rejects.toThrow('Failed to create empty RegistrySet');
    });

    it('should throw if profile can not be updated', async () => {
      const url = new URL('https://example.com/some/path/to/profile');

      const mockFetch = vi.fn();
      mockFetch.mockResolvedValueOnce({ ok: true });
      mockFetch.mockResolvedValueOnce({ ok: false });

      const registrySet = new RegistrySet(url, mockFetch);

      await expect(registrySet.createRegistrySet()).rejects.toThrow(
        'Failed to update profile with new RegistrySet URL'
      );
    });
  });

  describe('dataRegistries', () => {
    it('should throw if no registry can be found', async () => {
      const url = new URL('https://example.com/some/path/to/profile');

      const mockFetch = vi.fn();
      mockFetch.mockResolvedValue({ ok: true, text: vi.fn() });

      const registrySet = new RegistrySet(url, mockFetch);
      await expect(registrySet.dataRegistries).rejects.toThrow('No registry of type DataRegistry');
    });
  });
});
