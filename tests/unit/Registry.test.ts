import { afterEach, describe, expect, it, vi } from 'vitest';
import { Registry } from '../../src/Registry';

import * as N3 from 'n3';

const mockParser = { parse: vi.fn() };
// const mockStore = vi.mocked<import('n3').Store>({ getQuads: vi.fn() } as any);
// const mockWriter = vi.mocked<import('n3').Writer>({ addQuad: vi.fn(), end: vi.fn() } as any);
const mockStore = { getQuads: vi.fn() } as any;
const mockWriter = { addQuad: vi.fn(), list: vi.fn(), end: (fn) => fn(undefined, 'fake registry data') } as any;

vi.mock('n3');

const spyParser = vi.spyOn(N3, 'Parser').mockImplementation(() => mockParser);
const spyStore = vi.spyOn(N3, 'Store').mockImplementation(() => mockStore);
const spyWriter = vi.spyOn(N3, 'Writer').mockImplementation(() => mockWriter);

vi.mock(
  'uuid',
  vi.fn(() => ({ v4: () => 'fake-uuid' }))
);

vi.mock('uuid', () => ({ v4: () => 'fake-uuid' }));
vi.mock('../../src/templates', () => ({
  registrationTemplate: async () => 'fake registration template',
}));

describe('Registry', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('should create a new instance', () => {
    const url = new URL('https://example.com/some/path/to/registry');
    const typeURL = new URL('https://type.example.com/SomeRegistry');
    const registrySetURL = new URL('https://example.com/some/path/to/registryset');

    const mockFetch = vi.fn();

    const registry = new Registry(url, typeURL, registrySetURL, mockFetch);

    expect(registry).toBeDefined();

    expect(spyParser).toHaveBeenCalledOnce();
    expect(spyParser).toBeCalledWith({ baseIRI: 'https://example.com/some/path/to/registry' });
  });

  it('should initialize', async () => {
    const url = new URL('https://example.com/some/path/to/registry');
    const typeURL = new URL('https://type.example.com/SomeRegistry');
    const registrySetURL = new URL('https://example.com/some/path/to/registryset');

    const mockFetch = vi.fn();
    mockFetch.mockResolvedValue({ ok: true, text: vi.fn() });

    const registry = new Registry(url, typeURL, registrySetURL, mockFetch);

    await registry.init();

    expect(mockFetch).toBeCalledTimes(1);
    expect(mockFetch).toBeCalledWith(url, { headers: { Accept: 'text/turtle' } });
    expect(mockParser.parse).toBeCalledTimes(1);

    expect(registry._store).toBe(mockStore);
  });

  it('should only initialize once', async () => {
    const url = new URL('https://example.com/some/path/to/registry');
    const typeURL = new URL('https://type.example.com/SomeRegistry');
    const registrySetURL = new URL('https://example.com/some/path/to/registryset');

    const mockFetch = vi.fn();
    mockFetch.mockResolvedValue({ ok: true, text: vi.fn() });

    const registry = new Registry(url, typeURL, registrySetURL, mockFetch);

    await registry.init();

    expect(mockFetch).toBeCalledTimes(1);
    expect(mockFetch).toBeCalledWith(url, { headers: { Accept: 'text/turtle' } });
    expect(mockParser.parse).toBeCalledTimes(1);

    expect(registry._store).toBeDefined();

    await registry.init();

    expect(mockFetch).toBeCalledTimes(1);
  });

  describe('registrations.find', () => {
    it('should find single registration with shape as URL', async () => {
      const url = new URL('https://example.com/some/path/to/registry');
      const typeURL = new URL('https://type.example.com/SomeRegistry');
      const registrySetURL = new URL('https://example.com/some/path/to/registryset');
      const shape = new URL('https://shape.example.com/TestShape');
      const registrationURL = new URL('https://example.com/some/path/to/firstregistration');

      const mockFetch = vi.fn();
      mockFetch.mockResolvedValue({ ok: true, text: vi.fn() });

      mockStore.getQuads.mockReturnValueOnce([{ object: { value: registrationURL.toString() } }]);
      mockStore.getQuads.mockReturnValueOnce([{}]);

      const registry = new Registry(url, typeURL, registrySetURL, mockFetch);
      const registrations = await registry.registrations.find(shape);

      expect(registrations).toHaveLength(1);

      const registration = registrations[0];

      expect(registration.url).toStrictEqual(registrationURL);
    });

    it('should find single registration with shape as string', async () => {
      const url = new URL('https://example.com/some/path/to/registry');
      const typeURL = new URL('https://type.example.com/SomeRegistry');
      const registrySetURL = new URL('https://example.com/some/path/to/registryset');
      const shape = 'TestShape';
      const registrationURL = new URL('https://example.com/some/path/to/firstregistration');

      const mockFetch = vi.fn();
      mockFetch.mockResolvedValue({ ok: true, text: vi.fn() });

      mockStore.getQuads.mockReturnValueOnce([{ object: { value: registrationURL.toString() } }]);
      mockStore.getQuads.mockReturnValueOnce([{}]);

      const registry = new Registry(url, typeURL, registrySetURL, mockFetch);
      const registrations = await registry.registrations.find(shape);

      expect(registrations).toHaveLength(1);

      const registration = registrations[0];

      expect(registration.url).toStrictEqual(registrationURL);
    });
  });

  describe('registrations.create', () => {
    it('should create registration with shape as URL', async () => {
      const url = new URL('https://example.com/some/path/to/registry');
      const typeURL = new URL('https://type.example.com/SomeRegistry');
      const registrySetURL = new URL('https://example.com/some/path/to/registryset');
      const shape = new URL('https://shape.example.com/TestShape');
      const registrationURL = new URL('https://example.com/some/path/to/fake-uuid');

      const mockFetch = vi.fn();
      mockFetch.mockResolvedValue({ ok: true, text: vi.fn() });

      mockStore.getQuads.mockReturnValueOnce([{ object: {} }]);

      mockStore[Symbol.iterator] = function* () {
        yield { subject: 'some sub', object: 'some obj', predicate: { equals: () => true } };
        yield { predicate: { equals: () => false } };
      };

      const registry = new Registry(url, typeURL, registrySetURL, mockFetch);

      await registry.registrations.create(shape);

      expect(mockFetch).toBeCalledWith(registrationURL, {
        method: 'PUT',
        body: 'fake registration template',
        headers: { 'Content-Type': 'text/turtle' },
      });

      expect(spyWriter).toBeCalledWith({
        prefixes: {
          xsd: 'http://www.w3.org/2001/XMLSchema#',
          ldp: 'http://www.w3.org/ns/ldp#',
          interop: 'http://www.w3.org/ns/solid/interop#',
          shapetree: 'http://data.example/shapetree/pm#',
        },
      });

      expect(mockFetch).toBeCalledWith(registrationURL, {
        method: 'PUT',
        body: 'fake registry data',
        headers: { 'Content-Type': 'text/turtle' },
      });
    });
  });
});
