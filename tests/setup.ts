// Required for MSW to work.
import nodeFetch from 'node-fetch';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
global.fetch = nodeFetch as any;
