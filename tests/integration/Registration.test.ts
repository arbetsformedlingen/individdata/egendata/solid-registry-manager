import { afterAll, afterEach, beforeAll, describe, expect, it, vi } from 'vitest';
import { setupServer } from 'msw/node';
import { Registration } from '../../src/Registration';
import { DataFactory } from 'n3';
import { a } from '../../src/templates';

import regularFetch from 'node-fetch';

import { handlers } from '../handlers.js';
import { rest } from 'msw';

const { namedNode, quad } = DataFactory;

vi.mock('uuid', () => ({
  v4: vi.fn(() => '751029b6-fe0e-43fd-9fe8-ab57ef1bc9b1'),
}));

beforeAll(() => {
  server.listen();
});

afterAll(() => {
  server.close();
  vi.resetAllMocks();
});

afterEach(() => {
  server.resetHandlers();
});

// const server = setupServer();
const server = setupServer(...handlers);

describe('Registration', () => {
  describe('constructor', () => {
    it('should return', () => {
      const url = new URL('http://pod.example.com/egendata/data/f33ae00d-8f62-46ed-aac8-40335886e7e2');

      const registration = new Registration(url, regularFetch as typeof fetch);

      expect(registration).toBeDefined();
    });
  });

  describe('children.find', () => {
    it('should return a list', async () => {
      const url = new URL('http://pod.example.com/egendata/data/f33ae00d-8f62-46ed-aac8-40335886e7e2');
      const registration = new Registration(url, regularFetch as typeof fetch);

      const search = {
        pred: 'https://egendata.se/core/v1#documentType',
        obj: 'https://egendata.se/core/v1#JobSeekerRegistrationStatus',
      };

      const children = await registration.children.find(search);

      expect(children).toHaveLength(1);
      // expect(children).toBe();
    });

    it('should handle only predicate as filter', async () => {
      const url = new URL('http://pod.example.com/egendata/data/f33ae00d-8f62-46ed-aac8-40335886e7e2');
      const registration = new Registration(url, regularFetch as typeof fetch);

      const search = {
        pred: 'https://egendata.se/core/v1#documentType',
      };

      const children = await registration.children.find(search);

      expect(children).toHaveLength(2);
    });

    it('should handle only object as filter', async () => {
      const url = new URL('http://pod.example.com/egendata/data/f33ae00d-8f62-46ed-aac8-40335886e7e2');
      const registration = new Registration(url, regularFetch as typeof fetch);

      const search = {
        obj: 'https://egendata.se/core/v1#JobSeekerRegistrationStatus',
      };

      const children = await registration.children.find(search);

      expect(children).toHaveLength(1);
    });
  });

  describe('children.first', () => {
    it('should return single string', async () => {
      const url = new URL('http://pod.example.com/egendata/data/f33ae00d-8f62-46ed-aac8-40335886e7e2');
      const registration = new Registration(url, regularFetch as typeof fetch);

      const search = {
        pred: 'https://egendata.se/core/v1#documentType',
        obj: 'https://egendata.se/core/v1#JobSeekerRegistrationStatus',
      };

      const child = await registration.children.first(search);

      expect(child).toBeDefined();
    });
  });

  describe('children.create', () => {
    it('should create a new child', async () => {
      server.use(
        rest.put('http://pod.example.com/egendata/data/751029b6-fe0e-43fd-9fe8-ab57ef1bc9b1', (req, res, ctx) => {
          return res(req.headers.get('Content-Type') === 'text/turtle' ? ctx.status(201) : ctx.status(400));
        }),
        rest.put('http://pod.example.com/egendata/data/f33ae00d-8f62-46ed-aac8-40335886e7e2', (req, res, ctx) => {
          return res(req.headers.get('Content-Type') === 'text/turtle' ? ctx.status(201) : ctx.status(400));
        })
      );

      const url = new URL('http://pod.example.com/egendata/data/f33ae00d-8f62-46ed-aac8-40335886e7e2');
      const registration = new Registration(url, regularFetch as typeof fetch);

      const prefixes = { egendata: 'https://egendata.se/core/v1#', xsd: 'http://www.w3.org/2001/XMLSchema#' };

      const quads = [
        quad(namedNode(''), a, namedNode('https://egendata.se/core/v1#DocumentTypeMetadata')),
        quad(
          namedNode(''),
          namedNode('https://egendata.se/core/v1#documentType'),
          namedNode('https://egendata.se/core/v1#JobSeekerRegistrationStatus')
        ),
      ];

      await expect(registration.children.create(quads, prefixes)).resolves.not.toThrow();
    });
  });
});
