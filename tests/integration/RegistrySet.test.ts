import { afterAll, afterEach, beforeAll, describe, expect, it, vi } from 'vitest';
import { setupServer } from 'msw/node';
import { RegistrySet } from '../../src/RegistrySet';

import regularFetch from 'node-fetch';

import { handlers } from '../handlers.js';
import { rest } from 'msw';

beforeAll(() => {
  server.listen();
});

afterAll(() => {
  server.close();
  vi.resetAllMocks();
});

afterEach(() => {
  server.resetHandlers();
});

const server = setupServer(...handlers);

describe('RegistrySet', () => {
  describe('constructor', () => {
    it('should create new objects', () => {
      const webid = new URL('http://pod.example.com/egendata/profile/card#me');
      const registrySet = new RegistrySet(webid, regularFetch as typeof fetch);

      expect(registrySet).toBeInstanceOf(RegistrySet);
    });

    it('should add dataRegistry property', async () => {
      server.use(
        rest.post('http://pod.example.com/egendata/profile/card', (req, res, ctx) => {
          return res(ctx.status(205));
        })
      );

      const webid = new URL('http://pod.example.com/egendata/profile/card#me');

      const dataRegistry = (await new RegistrySet(webid, regularFetch as typeof fetch).dataRegistries)[0];
      expect(dataRegistry).toBeDefined();
      expect(dataRegistry.url.toString()).toBe('http://pod.example.com/egendata/data/registry');
    });

    it('should throw if Registry Set can not be written', async () => {
      server.use(
        rest.put('http://pod.example.com/egendata-noregistryset/registryset', (req, res, ctx) => {
          return res(ctx.status(400));
        }),
        rest.post('http://pod.example.com/egendata-noregistryset/profile/card', (req, res, ctx) => {
          return res(ctx.status(400));
        })
      );

      const webid = new URL('http://pod.example.com/egendata-noregistryset/profile/card#me');

      await expect(new RegistrySet(webid, regularFetch as typeof fetch).dataRegistries).rejects.toThrow(
        'Failed to create empty RegistrySet'
      );
    });

    it.skip('should throw if profile can not be updated when creating Registry Set', async () => {
      server.use(
        rest.put('http://pod.example.com/egendata-noregistryset/registryset', (req, res, ctx) => {
          return res(req.headers.get('Content-Type') === 'text/turtle' ? ctx.status(201) : ctx.status(400));
        }),
        rest.put('http://pod.example.com/egendata-noregistryset/profile/card', (req, res, ctx) => {
          return res(ctx.status(400));
        })
      );

      const webid = new URL('http://pod.example.com/egendata-noregistryset/profile/card#me');

      await expect(new RegistrySet(webid, regularFetch as typeof fetch).dataRegistries).rejects.toThrow(
        'Failed to update profile with new RegistrySet URL'
      );
    });
  });
});
