import { afterAll, afterEach, beforeAll, describe, expect, it, vi } from 'vitest';
import { Registry } from '../../src/Registry';

import regularFetch from 'node-fetch';

import { setupServer } from 'msw/node';
import { rest } from 'msw';
import { handlers } from '../handlers';

vi.mock('uuid', () => ({
  v4: vi.fn(() => '751029b6-fe0e-43fd-9fe8-ab57ef1bc9b1'),
}));

beforeAll(() => {
  server.listen();
});

afterAll(() => {
  server.close();
  vi.resetAllMocks();
});

afterEach(() => {
  server.resetHandlers();
});

// const server = setupServer();
const server = setupServer(...handlers);

describe('Registry', () => {
  describe('constructor', () => {
    it('should return', () => {
      const url = new URL('http://pod.example.com/egendata/data/registry');
      const type = new URL('http://www.w3.org/ns/solid/interop#DataRegistry');
      const registrySetURL = new URL('http://pod.example.com/egendata/registryset');
      const registry = new Registry(url, type, registrySetURL, regularFetch as typeof fetch);

      expect(registry).toBeDefined();
    });
  });

  describe('registrations.find', () => {
    it('should be able to find Registration of given shape', async () => {
      const url = new URL('http://pod.example.com/egendata/data/registry');
      const type = new URL('http://www.w3.org/ns/solid/interop#DataRegistry');
      const registrySetURL = new URL('http://pod.example.com/egendata/registryset');
      const registry = new Registry(url, type, registrySetURL, regularFetch as typeof fetch);

      const shapeTree = new URL('http://data.example.com/shapetree/pm#DocumentTypeMetadata');

      const registration = await registry.registrations.find(shapeTree);

      expect(registration).toHaveLength(1);
      expect(registration[0].url.toString()).toBe(
        'http://pod.example.com/egendata/data/f33ae00d-8f62-46ed-aac8-40335886e7e2'
      );
    });
  });

  describe('registrations.create', () => {
    it('should create new Registration of given shape', async () => {
      server.use(
        rest.put('http://pod.example.com/egendata/data/751029b6-fe0e-43fd-9fe8-ab57ef1bc9b1', (req, res, ctx) => {
          return res(req.headers.get('Content-Type') === 'text/turtle' ? ctx.status(201) : ctx.status(400));
        }),
        rest.put('http://pod.example.com/egendata/data/registry', (req, res, ctx) => {
          return res(req.headers.get('Content-Type') === 'text/turtle' ? ctx.status(201) : ctx.status(400));
        })
      );

      const url = new URL('http://pod.example.com/egendata/data/registry');
      const type = new URL('http://www.w3.org/ns/solid/interop#DataRegistry');
      const registrySetURL = new URL('http://pod.example.com/egendata/registryset');
      const registry = new Registry(url, type, registrySetURL, regularFetch as typeof fetch);

      const shapeTree = new URL('http://data.example/shapetree/pm#DocumentTypeMetadata');

      await expect(registry.registrations.create(shapeTree)).resolves.not.toThrow();
    });
  });
});
