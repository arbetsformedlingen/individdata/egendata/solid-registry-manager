import { describe, expect, it } from 'vitest';
import { registrationTemplate, registrySetTemplate } from '../../src/templates';

describe('registrySetTemplate', () => {
  it('should return correct data', async () => {
    const data = await registrySetTemplate();

    const expected = `@prefix interop: <http://www.w3.org/ns/solid/interop#>.

<> a interop:RegistrySet.
`;

    expect(data).toBe(expected);
  });
});

describe('registrationTemplate', () => {
  it('should return correct data', async () => {
    const shape = new URL('http://data.example/shapetree/pm#RequestMetadata');
    const ts = new Date('2023-04-28T07:16:54.236Z');
    const data = await registrationTemplate(shape, ts);

    const expected = `@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
@prefix ldp: <http://www.w3.org/ns/ldp#>.
@prefix interop: <http://www.w3.org/ns/solid/interop#>.
@prefix shapetree: <http://data.example/shapetree/pm#>.

<> a interop:DataRegistration;
    interop:registeredBy <http://localhost:3000/egendata/profile/card#me>;
    interop:registeredWith <http://localhost:3000/egendata/profile/card#me>;
    interop:registeredAt "2023-04-28T07:16:54.236Z"^^xsd:dateTime;
    interop:updatedAt "2023-04-28T07:16:54.236Z"^^xsd:dateTime;
    interop:registeredShapeTree shapetree:RequestMetadata.
`;

    expect(data).toBe(expected);
  });
});
