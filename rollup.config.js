import typescript from 'rollup-plugin-typescript';
import dts from 'rollup-plugin-dts';
import pkg from './package.json' assert { type: 'json' };

// Modules that should not be built into the package.
const external = ['commander', 'n3', 'uuid'];

const config = [
  // Build CommonJS and ES Module
  {
    input: './src/index.ts',
    external,
    plugins: [typescript({ tsconfig: 'tsconfig.build.json' })],
    output: [
      {
        // CommonJS output
        format: 'cjs',
        file: pkg.main,
        sourcemap: true,
      },
      {
        // ES Module output
        format: 'esm',
        file: pkg.module,
        sourcemap: true,
      },
    ],
  },

  // TypeScript definition file
  {
    input: './src/index.ts',
    external,
    output: { file: pkg.types, format: 'esm' },
    plugins: [dts()],
  },
];

export default config;
