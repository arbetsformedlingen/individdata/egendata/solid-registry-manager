import * as N3 from 'n3';
import { DataFactory } from 'n3';
const { literal, namedNode, quad } = DataFactory;

export const a = namedNode('http://www.w3.org/1999/02/22-rdf-syntax-ns#type');

export const registrySetTemplate = async (): Promise<string> => {
  return new Promise((resolve, reject) => {
    const writer = new N3.Writer({ prefixes: { interop: 'http://www.w3.org/ns/solid/interop#' } });

    writer.addQuad(namedNode(''), a, namedNode('http://www.w3.org/ns/solid/interop#RegistrySet'));

    writer.end((error, result) => {
      if (error) reject();

      resolve(result);
    });
  });
};

export const registrationTemplate = async (shape: URL, ts: Date | undefined = undefined): Promise<string> => {
  return new Promise((resolve, reject) => {
    if (!ts) ts = new Date();

    const writer = new N3.Writer({
      prefixes: {
        xsd: 'http://www.w3.org/2001/XMLSchema#',
        ldp: 'http://www.w3.org/ns/ldp#',
        interop: 'http://www.w3.org/ns/solid/interop#',
        shapetree: 'http://data.example/shapetree/pm#',
      },
    });
    writer.addQuad(quad(namedNode(''), a, namedNode('http://www.w3.org/ns/solid/interop#DataRegistration')));
    writer.addQuad(
      quad(
        namedNode(''),
        namedNode('http://www.w3.org/ns/solid/interop#registeredBy'),
        namedNode('http://localhost:3000/egendata/profile/card#me')
      )
    );
    writer.addQuad(
      quad(
        namedNode(''),
        namedNode('http://www.w3.org/ns/solid/interop#registeredWith'),
        namedNode('http://localhost:3000/egendata/profile/card#me')
      )
    );
    writer.addQuad(
      quad(
        namedNode(''),
        namedNode('http://www.w3.org/ns/solid/interop#registeredAt'),
        literal(ts.toISOString(), namedNode('http://www.w3.org/2001/XMLSchema#dateTime'))
      )
    );
    writer.addQuad(
      quad(
        namedNode(''),
        namedNode('http://www.w3.org/ns/solid/interop#updatedAt'),
        literal(ts.toISOString(), namedNode('http://www.w3.org/2001/XMLSchema#dateTime'))
      )
    );
    writer.addQuad(
      quad(
        namedNode(''),
        namedNode('http://www.w3.org/ns/solid/interop#registeredShapeTree'),
        namedNode(shape.toString())
      )
    );
    writer.end((error, result) => {
      if (error) reject();

      resolve(result);
    });
  });
};
