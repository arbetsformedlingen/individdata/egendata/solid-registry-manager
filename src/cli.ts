import { program } from 'commander';
// import { fetchFactory } from './util/fetch';

// const fetch = fetchFactory();

program.name('egendata-interop').description('Handle Solid based registries with ease.').version('0.1.0');

program
  .command('list')
  .argument('<url>', 'URL of registry set')
  .description('List all registries in set')
  .action((url) => {
    url = new URL(url);

    console.log(url);
  });

program.parse();
