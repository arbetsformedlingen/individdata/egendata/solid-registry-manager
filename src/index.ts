// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type FetchLike = (url: any, options?: object) => Promise<Response>;

export { RegistrySet } from './RegistrySet';
export { Registry } from './Registry';
export { Registration } from './Registration';
