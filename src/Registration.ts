import * as N3 from 'n3';
import { Quad } from 'n3';
import { v4 as uuid } from 'uuid';
import { FetchLike } from '.';

export class Registration {
  _fetch: FetchLike;
  _parser: N3.Parser;
  _store: N3.Store | undefined;
  _url: URL;
  _registeredShapeTree: URL | undefined;

  constructor(url: URL, fetchFn: FetchLike) {
    this._fetch = fetchFn;
    this._parser = new N3.Parser({ baseIRI: url.toString() });
    this._url = url;
  }

  get url() {
    return this._url;
  }

  async init() {
    if (this._store) return;

    const response = await this._fetch(this.url, { headers: { Accept: 'text/turtle' } });
    if (!response.ok) {
      throw new Error(`Failed to init Registration object, (${response.statusText} [${response.status}])`);
    }
    this._store = new N3.Store(this._parser.parse(await response.text()));

    const quad = this._store.getQuads(null, 'http://www.w3.org/ns/solid/interop#registeredShapeTree', null, null);
    if (quad.length !== 1) throw new Error(`Missing or multiple ShapeTrees in Registration (${this._url})`);

    this._registeredShapeTree = new URL(quad[0].object.value);
  }

  get children() {
    return {
      find: (search: { pred?: string; obj?: string }) => this.find(search),
      first: (search: { pred?: string; obj?: string }) => this.find(search).then((list) => list[0]),
      create: (quads: Quad[], prefixes = {}) => this.create(quads, prefixes),
    };
  }

  private async find(search: { pred?: string; obj?: string }): Promise<N3.Store[]> {
    if (!this._store) await this.init();

    const { pred, obj } = search;

    const urls =
      this._store
        ?.getQuads(null, 'http://www.w3.org/ns/ldp#contains', null, null)
        .map((q) => new URL(q.object.value)) ?? [];

    const rv = [];

    for (const url of urls) {
      const response = await this._fetch(url);
      if (response.ok) {
        const store = new N3.Store(this._parser.parse(await response.text()));
        const quads = store.getQuads(null, pred ?? null, obj ?? null, null);

        if (quads.length > 0) rv.push(store);
      }
    }

    return rv;
  }

  private async create(quads: Quad[], prefixes: { [key: string]: string }) {
    if (!this._store) await this.init();

    // Generate new URL
    const url = new URL(uuid(), this._url);

    // Generate new resource
    const newChild: string = await new Promise((resolve, reject) => {
      const writer = new N3.Writer({ prefixes });
      for (const quad of quads) writer.addQuad(quad);

      writer.end((error, result) => {
        if (error) reject();

        resolve(result);
      });
    });

    const newResourceResponse = await this._fetch(url, {
      method: 'PUT',
      body: newChild,
      headers: { 'Content-Type': 'text/turtle' },
    });

    if (!newResourceResponse.ok)
      throw new Error(
        `Failed to write new resource (${newResourceResponse.statusText} [${newResourceResponse.status}])`
      );
  }
}
