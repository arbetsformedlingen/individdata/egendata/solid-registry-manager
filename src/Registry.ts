import * as N3 from 'n3';
import { DataFactory } from 'n3';
import { v4 as uuid } from 'uuid';

import { Registration } from './Registration';

const { namedNode } = DataFactory;

import { registrationTemplate } from './templates';

export class Registry {
  _fetch: typeof fetch;
  _store: N3.Store | undefined;
  _parser: N3.Parser;
  _registryType: URL;
  _url: URL;
  _registrySet: URL;

  constructor(url: URL, type: URL, registrySet: URL, fetchFn: typeof fetch) {
    this._fetch = fetchFn;
    this._registryType = type;
    this._registrySet = registrySet;
    this._parser = new N3.Parser({ baseIRI: url.toString() });
    this._url = url;
  }

  get url() {
    return this._url;
  }

  async init() {
    if (this._store) return;

    const response = await this._fetch(this._url, { headers: { Accept: 'text/turtle' } });
    this._store = new N3.Store(this._parser.parse(await response.text()));
  }

  // async getRegistrations() {
  //   if (!this._store) await this.init();

  //   const quads = this._store?.getQuads(null, 'http://www.w3.org/ns/solid/interop#hasDataRegistration', null, null);
  //   return quads?.map((dr) => new URL(dr.object.value));
  // }

  get registrations() {
    return {
      find: (search: string | URL) => this.findRegistrations(search),
      create: (shape: string | URL) => this.createRegistration(shape),
    };
  }

  // get instances() {
  //   return {
  //     find: (...args) => this.findInstances(...args),
  //   };
  // }

  private async findRegistrations(shape: string | URL): Promise<Registration[]> {
    if (!this._store) await this.init();

    if (typeof shape === 'string') shape = new URL(`http://data.example.com/shapetree/pm#${shape}`);

    const urls =
      this._store
        ?.getQuads(null, 'http://www.w3.org/ns/solid/interop#hasDataRegistration', null, null)
        .map((q) => new URL(q.object.value)) ?? [];

    const rv = [];

    for (const url of urls) {
      const response = await this._fetch(url);
      if (response.ok) {
        const store = new N3.Store(this._parser.parse(await response.text()));

        const quad = store.getQuads(
          null,
          'http://www.w3.org/ns/solid/interop#registeredShapeTree',
          shape.toString(),
          null
        );

        if (quad.length === 1) {
          rv.push(new Registration(url, this._fetch));
        }
      }
    }

    return rv;
  }

  private async createRegistration(shape: string | URL): Promise<void> {
    if (!this._store) await this.init();

    if (typeof shape === 'string') shape = new URL(shape, 'http://data.example/shapetree/pm#');

    // Generate new URL
    const url = new URL(uuid(), this._url);

    // Write new resource
    const newResourceResponse = await this._fetch(url, {
      method: 'PUT',
      body: await registrationTemplate(shape),
      headers: { 'Content-Type': 'text/turtle' },
    });

    if (!newResourceResponse.ok)
      throw new Error(
        `Failed to create registration (${newResourceResponse.statusText} [${newResourceResponse.status}])`
      );

    // Update registry
    const updatedRegistry: string = await new Promise((resolve, reject) => {
      const writer = new N3.Writer({
        prefixes: {
          xsd: 'http://www.w3.org/2001/XMLSchema#',
          ldp: 'http://www.w3.org/ns/ldp#',
          interop: 'http://www.w3.org/ns/solid/interop#',
          shapetree: 'http://data.example/shapetree/pm#',
        },
      });

      // Add object to ldp:contains predicate and make sure it prints pretty
      const contains = (
        this._store?.getQuads(null, namedNode('http://www.w3.org/ns/ldp#contains'), null, null) ?? []
      ).map((q) => q.object);
      contains.push(namedNode(url.toString()));

      for (const quad of this._store ?? []) {
        if (!quad.predicate.equals(namedNode('http://www.w3.org/ns/ldp#contains'))) {
          writer.addQuad(quad);
        }
      }

      writer.addQuad(
        namedNode(this.url.toString()),
        namedNode('http://www.w3.org/ns/ldp#contains'),
        writer.list(contains)
      );

      writer.end((error, result) => {
        if (error) reject();

        resolve(result);
      });
    });

    const updatedRegistryResponse = await this._fetch(this._url, {
      method: 'PUT',
      body: updatedRegistry,
      headers: { 'Content-Type': 'text/turtle' },
    });

    if (!updatedRegistryResponse.ok)
      throw new Error(
        `Failed to update registry (${updatedRegistryResponse.statusText} [${updatedRegistryResponse.status}])`
      );
  }
}
