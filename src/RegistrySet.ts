import * as N3 from 'n3';

import { registrySetTemplate } from './templates';
import { Registry } from './Registry';

export class RegistrySet {
  static readonly REGISTRY_TYPES: { [key: string]: { predicate: URL; object: URL } } = {
    DataRegistry: {
      predicate: new URL('http://www.w3.org/ns/solid/interop#hasDataRegistry'),
      object: new URL('http://www.w3.org/ns/solid/interop#DataRegistry'),
    },
  };

  _fetch: typeof fetch;
  _store: N3.Store | undefined;
  _parser: N3.Parser;
  _profile: URL;
  _url: URL | undefined;

  constructor(webid: URL, fetchFn: typeof fetch) {
    this._profile = webid;

    this._fetch = fetchFn;
    this._parser = new N3.Parser({ baseIRI: new URL('', this._profile).toString() });
  }

  async init() {
    if (this._store) return;

    // Get URL of RegistrySet from profile document
    const profileResponse = await this._fetch(this._profile, { headers: { Accept: 'text/turtle' } });
    const store = new N3.Store(this._parser.parse(await profileResponse.text()));

    const registrySets = store
      .getQuads(null, 'http://www.w3.org/ns/solid/interop#hasRegistrySet', null, null)
      .map((quad) => new URL(quad.object.value));

    if (registrySets.length === 0) {
      registrySets.push(await this.createRegistrySet());
    }

    // Assume single Registry Set, for now.
    this._url = registrySets[0];

    const registrySetResponse = await this._fetch(this._url);

    if (!registrySetResponse.ok) throw new Error('Failed to read Registry Set.');

    this._store = new N3.Store(this._parser.parse(await registrySetResponse.text()));
  }

  get dataRegistries() {
    return this.getRegistriesOfType('DataRegistry');
  }

  async createRegistrySet() {
    const registrySetURL = new URL('../registryset', new URL('', this._profile));
    const registrySetURLResponse = await this._fetch(registrySetURL, {
      method: 'PUT',
      body: await registrySetTemplate(),
      headers: { 'Content-Type': 'text/turtle' },
    });

    if (!registrySetURLResponse.ok) throw new Error('Failed to create empty RegistrySet');

    const updatedProfileResponse = await this._fetch(this._profile, {
      method: 'PATCH',
      body: `INSERT DATA { <${this._profile.toString()}> <http://www.w3.org/ns/solid/interop#hasRegistrySet> <${registrySetURL.toString()}>.}`,
      headers: { 'Content-Type': 'application/sparql-update' },
    });

    if (!updatedProfileResponse.ok) throw new Error('Failed to update profile with new RegistrySet URL');

    return registrySetURL;
  }

  private async getRegistriesOfType(type: string) {
    if (!this._store) await this.init();

    const { predicate, object } = RegistrySet.REGISTRY_TYPES[type];
    const quads = this._store?.getQuads(null, predicate.toString(), null, null);

    if (quads?.length === 0) throw new Error(`No registry of type ${type}`);

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return quads?.map((quad) => new Registry(new URL(quad.object.value), object, this._url!, this._fetch));
  }
}
